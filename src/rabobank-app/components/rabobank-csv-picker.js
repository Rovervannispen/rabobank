  import {
    html,
    PolymerElement
  } from '@polymer/polymer/polymer-element.js';

  /**
   * @customElement
   * @polymer
   */
  class RabobankCsvPicker extends PolymerElement {
    static get template() {
      return html `
      <style>
        input[type="file"] {
          display: none;
        }
        .custom-file-upload {
          display: inline-block;
          background-color:white;
          padding: 6px 12px;
          border: 2px solid #070090;
          border-radius:10px;
          cursor: pointer;
        }
  
        .custom-file-upload:hover {
          background-color:#f8f6ff;
        }
        </style>
        <label for="csvUpload" class="custom-file-upload">
          Select .csv file...
        </label>
        <input id="csvUpload" type="file" on-change="_filePicked" accept="text/csv" />
      `;
    }
    static get properties() {
      return {
        csvData: {
          type: Array,
          notify: true, //notify up to make sure it's available in the parent component
          value: function () {
            return []; //return empty array when no csv file has been selected yet.
          }
        },
        headers: {
          type: Array,
          notify: true, //notify up to make sure it's available in the parent component
          value: function () {
            return []; //return empty array when no csv file has been selected yet.
          }
        }
      };
    }

    /**
     * Gets triggered when the user picks a file or changes the selected file.
     * Do an extra check to make sure it can only handle CSV files (also checked in input element )
     * @param {Event} e 
     */
    _filePicked(e) {
      const file = this.$.csvUpload.files[0];
      if (file.type === "text/csv") {
        this._readFile(file);
      } else {
        alert("Sorry, only CSV files allowed");
      }
    }

    /**
     * Creates a new filereader to read a csv file as text and converts the results 
     * @param {File} file - gets data from input with type 'file'
     */
    _readFile(file) {
      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = event => {
        const result = event.target.result.replace(/"/g, ""); // removes all double quotes 
        this.set("csvData", this.csvToJson(result));
      };

      reader.onerror = function () {
        alert('Unable to read ' + file.fileName);
      };
    }

    /**
     * Takes a CSV string and returns an array of objects with CSV content.
     * 
     * @param {String} csv - takes a unformatted string
     * @returns {Array} result - returns an array with key/value pairs based on csv content and matching headers. 
     */
    csvToJson(csv) {

      let [headers, ...rows] = csv.split(/\n/); // destruct header row and content rows
      const result = []; // declare result as constant so it can't be altered while converting to JSON

      /**
       * 1) replace all spaces - and carriage returns - in key names
       * 2) not necessary but since we're going to use the headers as keys it's cleaner to only use lowercase chars.
       * 3) split string to create an array of header keys
       */

      headers = headers.replace(/[ \r]/g, "").toLowerCase().split(",");

      this.set("headers", headers); // set headers and push up to parent component to render DOM

      //loop over all rows, find matching headers and push new object to results array
      rows.forEach((row, index) => {
        let obj = {},
          current = row.split(',');
        headers.forEach((header, index) => obj[header] = current[index]);
        result.push(obj);
      });
      return result;
    }
  }

  window.customElements.define('rabobank-csv-picker', RabobankCsvPicker);