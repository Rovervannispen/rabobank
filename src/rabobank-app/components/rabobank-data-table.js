  import {
    html,
    PolymerElement
  } from '@polymer/polymer/polymer-element.js';
  import '@polymer/polymer/lib/elements/dom-repeat';

  /**
   * @customElement
   * @polymer
   */
  class RabobankDataTable extends PolymerElement {
    static get template() {
      return html `
      <style>
        table {
            width: 100%;
            border-radius: 10px;
            box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15)
        }

        table tr {
            background-color: white;
        }

        table tr:nth-child(even) {
            background-color: #f8f6ff;
        }

        table th {
            font-size: 18px;
            color: #fff;
            line-height: 1.4;
            background-color: #080093;
            font-weight: 100;
        }

        table td {
            text-align: center;
        }

        input {
            font-size: 1em;
            margin: 1em 0;
            padding: 6px 12px;
            border: 2px solid #80BA27;
            border-radius: 10px;
            width: 100%;
        }

        .filters {
            display: flex;
            width: 100%;
        }

        .filters input:nth-of-type(1) {
            margin-right: 8px;
        }

        .filters input:nth-of-type(2) {
            margin-left: 8px;
        }
      </style>
      <div class="filters">
          <input value="{{minIssueInput::input}}" placeholder="Min issue count">
          <input value="{{maxIssueInput::input}}" placeholder="Max issue count">
      </div>
      <table>
          <thead>
              <tr>
                  <template is="dom-repeat" items="[[headers]]">
                      <th>[[item]]</th>
                  </template>
              </tr>
          </thead>
          <template is="dom-repeat" items="[[csvData]]" filter="{{minIssueCount(minIssueInput, maxIssueInput)}}">
              <tr>
                  <td>[[item.firstname]]</td>
                  <td>[[item.surname]]</td>
                  <td>[[item.issuecount]]</td>
                  <td>[[item.dateofbirth]]</td>
              </tr>
          </template>
      </table>
      `;
    }
    static get properties() {
      return {};
    }

    /**
     * Adds a filter to the dom-repeat. Everytime a user enters a char this filter will run over all the items and will return matching ones.
     * @param {String} minCount - min count to filter on
     * @returns {Boolean} - return true/false when an item meets one or two criterias
     */
    minIssueCount(minCount, maxCount) {
      if (!minCount && !maxCount) {
        //disable filter if both values are not filled in yet
        return null;
      } else {
        return function (user) {
          /**
           * To make sure this filter will also work when only one of the input fields is filled in
           * check if eiter minCount or maxCount is filled in.
           * 
           * issueCount and minCount are strings, make sure to parse to an integer to do proper checking
           */
          if (minCount && !maxCount) {
            return parseInt(user.issuecount) >= parseInt(minCount);
          } else if (!minCount && maxCount) {
            return parseInt(user.issuecount) <= parseInt(maxCount);
          }
          return parseInt(user.issuecount) >= parseInt(minCount) && parseInt(user.issuecount) <= parseInt(maxCount);
        };
      }
    }
  }

  window.customElements.define('rabobank-data-table', RabobankDataTable);