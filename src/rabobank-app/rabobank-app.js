import {
  html,
  PolymerElement
} from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if';
import './components/rabobank-csv-picker';
import './components/rabobank-data-table';


/**
 * @customElement
 * @polymer
 */
class RabobankApp extends PolymerElement {
  static get template() {
    return html `
    <style>
      :host {
          display: flex;
          justify-content: center;
      }

      .app {
          font-family: Lato;
          width: 800px;
      }

      .app__logo,
      .app__filepicker {
          display: flex;
          justify-content: center;
      }

      .app__logo,
      .app__filepicker,
      .app__gridview {
          margin: 1em 0;
      }

      .app__intro {
          background-color: #fff;
          box-shadow: 0 0px 40px 0px rgba(0, 0, 0, 0.15);
          border-radius: 10px;
          padding: 1em 0;
          margin: 2em 0;
      }

      .app__intro p.intro__step {
          text-align: center;
          margin-bottom: 8px;
      }

      .app__intro p.intro__step strong {
          display: block;
      }

      .app__intro {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
      }

      rabobank-data-table {
          display: flex;
          flex-direction: column;
          align-items: center;
      }
    </style>

    <div class="app">
        <div class="app__logo">
            <img src="https://www.rabobank.nl/static/generic/css/images/s14/rabobank-logo.png" />
        </div>
        <div class="app__intro">
            <p class="intro__step">
                <strong>Step 1:</strong>
                Click the 'Select .csv file' button
            </p>
            <p class="intro__step">
                <strong>Step 2:</strong>
                Browse on select the file you want to upload
            </p>
            <p class="intro__step">
                <strong>Step 3:</strong>
                Click 'open'
            </p>
            <p class="intro__step">
                <strong>Step 4:</strong>
                Enter a minumum issue count
            <p>
        </div>

        <div class="app__filepicker">
            <rabobank-csv-picker csv-data="{{csvData}}" headers="{{headers}}"></rabobank-csv-picker>
        </div>
        <div class="app__gridview">
            <template is="dom-if" if="[[_gotData(csvData)]]">
                <rabobank-data-table csv-data="[[csvData]]" headers="[[headers]]"></rabobank-data-table>
            </template>
        </div>
    </div>`;
  }
  static get properties() {
    return {};
  }

  /**
   * 
   * @param {Array} data - takes an array and checks whether there is data or not
   * @returns {Boolean} - returns true when there is data, false when not
   */
  _gotData(data) {
    return data.length > 0;
  }
}

window.customElements.define('rabobank-app', RabobankApp);